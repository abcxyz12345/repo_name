package com.billpay.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "account_id")
	private Long accountId;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="customer_id")
	public Customer customer;
			
	@Column(name = "branch_id")
	private Long branchId;
	
	@Column(name = "account_type")
	private String accountType;
	
	@Column(name = "account_number")
	private Long accountNumber;
	
	@Column(name = "billpay_enabled")
	private Long billpayEnabled;
	
	@Column(name = "balance_amount")
	private Long balanceAmount;

	
	
	public Long getAccountId() {
		return accountId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long long1) {
		this.accountNumber = long1;
	}

	public Long getBillpayEnabled() {
		return billpayEnabled;
	}

	public void setBillpayEnabled(Long billpayEnabled) {
		this.billpayEnabled = billpayEnabled;
	}

	public Long getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	
	
}
