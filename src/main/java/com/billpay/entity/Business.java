package com.billpay.entity;

import java.math.BigDecimal;
import java.util.Date;
//import java.sql.Date;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "business")
public class Business {
	
	@Id
	@Column(name = "business_id")
	private Long businessId;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="customer_id")
	public Customer customer;
	
	@Column(name = "name")
	private String name1;
	
	@Column(name = "dayofmonth")
	private String dayOfMonth;

	
	
	@Column(name = "address")
	private String address1;
	
	@Column(name = "paytotheorderof")
	private String payToTheOrderOf;
	
	@Column(name = "chequestartdate")
	private Date checkSendDate;
	
	public Long getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getDayOfMonth() {
		return dayOfMonth;
	}

	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getPayToTheOrderOf() {
		return payToTheOrderOf;
	}

	public void setPayToTheOrderOf(String payToTheOrderOf) {
		this.payToTheOrderOf = payToTheOrderOf;
	}

	public Date getCheckSendDate() {
		return checkSendDate;
	}

	public void setCheckSendDate(Date checkSendDate) {
		this.checkSendDate = checkSendDate;
	}

	public BigDecimal getCheckAmount() {
		return checkAmount;
	}

	public void setCheckAmount(BigDecimal checkAmount) {
		this.checkAmount = checkAmount;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	@Column(name = "chequeamount")
	private BigDecimal checkAmount;
	
	@Column(name = "frequency")
	private String frequency;
	
}
	
	