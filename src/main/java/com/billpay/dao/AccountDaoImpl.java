package com.billpay.dao;

import org.springframework.stereotype.Repository;

import com.billpay.entity.Account;
//import com.billpay.entity.Customer;
import com.billpay.dao.PagedDao;

@Repository
public class AccountDaoImpl extends PagedDao<Account> implements AccountDao {

	@Override
	public Account getAccount(Long accountId) {
		//int i = Integer.valueOf(accountId.toString());
		return get(accountId);
	}

	@Override
	public int saveAccount(Account account) {
		
		return save(account);
	}

	@Override
	public void updateAccount(Account account) {
		 update(account);
	}

	
	
}
