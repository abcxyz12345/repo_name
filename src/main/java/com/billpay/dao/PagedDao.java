package com.billpay.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public abstract class PagedDao<T> {
	public static int MAXIMUM_LIMIT = 100;

	@Autowired
	private SessionFactory sessionFactory;
	// @Autowired
	private Class<T> entity;

	/*
	 * public PagedDao(Class<T> clazz) { this.entity = clazz; }
	 */
	/*
	 * @SuppressWarnings("unchecked") public PagedDao(Class<?> classe) {
	 * this.entity = (Class<T>) classe; }
	 */

	/**
	 * Lists items given a first index indicator and the page limit size
	 * 
	 * @param limit
	 *            - The limit of results
	 * @param offset
	 *            - The starting point to return
	 * @return list of items of type T
	 */
	@SuppressWarnings("unchecked")
	protected List<T> list(int limit, int offset) {
		return sessionFactory.getCurrentSession()
				.createCriteria(classFromGenerics()).setFirstResult(offset)
				.setMaxResults(fitLimit(limit)).list();
	}

	/**
	 * Returns the element with the informed id of type T
	 * 
	 * @param id
	 *            - The id of the element in the table
	 * @return the element with the informed id of type T
	 */
	@SuppressWarnings("unchecked")
	protected T get(int id) {
		return (T) sessionFactory.getCurrentSession().get(classFromGenerics(),
				id);
	}

	@SuppressWarnings("unchecked")
	protected T get(Long id) {
		return (T) sessionFactory.getCurrentSession().get(classFromGenerics(),
				id);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<T> findAll() {
		return (List<T>) sessionFactory.getCurrentSession()
				.createCriteria(entity)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	/**
	 * Saves the registry
	 * 
	 * @param value
	 *            - the item to be saved
	 * @return the id of the saved item
	 */
	/*
	 * protected int save(T value){ return (int)sessionFactory
	 * .getCurrentSession() .save(value) }
	 */

	protected int save(T value) {
		sessionFactory.getCurrentSession().save(value);
		return 1;
	}

	/**
	 * Updates the registry
	 * 
	 * @param value
	 *            - the item to be saved
	 * @return the id of the saved item
	 */
	protected void update(T value) {
		sessionFactory.getCurrentSession().update(value);
	}

	/*
	 * @SuppressWarnings("unchecked") public List<T> getAll() { List<T> result =
	 * (List<T>) sessionFactory.getCurrentSession().createQuery("from " +
	 * entity.getName()).list(); return result; }
	 */

	@SuppressWarnings("unchecked")
	public <T> List<T> getAll(final Class<T> type) {
		final Criteria crit = sessionFactory.getCurrentSession()
				.createCriteria(type);
		return crit.list();
	}

	/**
	 * public List<T> getAll() { List<T> result = (List<T>)
	 * getSessionFactory().getCurrentSession().createQuery("from " +
	 * entity.getName()).list(); return result; }
	 * 
	 * @return the class object of type T
	 */
	@SuppressWarnings("unchecked")
	private Class<T> classFromGenerics() {
		return (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	private int fitLimit(int limit) {
		return (limit == 0 || limit > MAXIMUM_LIMIT) ? MAXIMUM_LIMIT : limit;
	}

}
