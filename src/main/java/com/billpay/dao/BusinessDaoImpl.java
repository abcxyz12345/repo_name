package com.billpay.dao;

import java.util.ArrayList;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.billpay.entity.Business;
//import com.billpay.entity.Customer;
import com.billpay.dao.PagedDao;

@Repository
public class BusinessDaoImpl extends PagedDao<Business> implements BusinessDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Business getBusiness(Long businessId) {
		return get(businessId);
	}

	@Override
	public int saveBusiness(Business business) {

		return save(business);
	}

	@Override
	public void updateBusiness(Business business) {
		update(business);
	}

	/*
	 * @Override public List<Business> get() { // TODO Auto-generated method
	 * stub
	 * 
	 * List<Business> businessList = new ArrayList<Business>(); businessList=
	 * sessionFactory.getCurrentSession().createQuery("from business").list();
	 * 
	 * 
	 * return businessList;
	 * 
	 * }
	 */

	/*
	 * @Override public List<Business> getAllEntities() { // TODO Auto-generated
	 * method stub List<Business> businessList = new ArrayList<Business>();
	 * businessList = getAll(); return businessList; }
	 */

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Business> getAllEntities() {
		return getAll(Business.class);
	}

	/*
	 * @Override public List<Business> getAllEntities() { // TODO Auto-generated
	 * method stub return null; }
	 */

	/*
	 * public List<Business> getAllEntities() { // TODO Auto-generated method
	 * stub List<Business> businessList = new ArrayList<Business>();
	 * businessList = getAll(); return businessList; }
	 */

}
