package com.billpay.dao;

import org.springframework.stereotype.Repository;

import com.billpay.entity.Customer;

@Repository
public class CustomerDaoImpl extends PagedDao<Customer> implements CustomerDao {
	
	
	@Override
	public Customer getCustomer(int customerId) {
		return get(customerId);
	}

	@Override
	public int saveCustomer(Customer customer) {
		
		return save(customer);
	}

	@Override
	public void updateCustomer(Customer customer) {
		 update(customer);
	}

	

}
