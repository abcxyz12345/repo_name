package com.billpay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.billpay.entity.Customer;

@Component
public interface CustomerDao {

	public Customer getCustomer(int customerId);
	
	public int saveCustomer(Customer customer);
	
	public void updateCustomer(Customer customer);
}
