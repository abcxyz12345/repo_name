package com.billpay.dao;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.billpay.entity.Transaction;

@Component
public interface TransactionDao {
	
public Transaction getTransaction(Long transactionId);
	
	public int saveTransaction(Transaction transaction);
	
	public void updateTransaction(Transaction transaction);
	

}
