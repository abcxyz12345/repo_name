package com.billpay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.billpay.entity.Business;

@Component
public interface BusinessDao {

	public Business getBusiness(Long id);

	public int saveBusiness(Business business);

	public void updateBusiness(Business business);

	public List<Business> getAllEntities();
	// public List<Business> findAllEntities();

}
