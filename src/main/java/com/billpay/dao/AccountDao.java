package com.billpay.dao;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.billpay.entity.Account;


@Component

public interface AccountDao {
public Account getAccount(Long id);
	
	public int saveAccount(Account account);
	
	public void updateAccount(Account account);

}
