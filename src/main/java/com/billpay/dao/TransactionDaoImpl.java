package com.billpay.dao;

import org.springframework.stereotype.Repository;

import com.billpay.entity.Transaction;
import com.billpay.entity.Transaction;

@Repository
public class TransactionDaoImpl extends PagedDao<Transaction> implements TransactionDao {
	
	@Override
	public Transaction getTransaction(Long transactionId) {
		return get(transactionId);
	}

	@Override
	public int saveTransaction(Transaction transaction) {
		
		return save(transaction);
	}

	@Override
	public void updateTransaction(Transaction transaction) {
		 update(transaction);
	}

	


}
