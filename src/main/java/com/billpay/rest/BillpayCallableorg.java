package com.billpay.rest;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.billpay.entity.Customer;
import com.billpay.dao.CustomerDao;
import com.billpay.dao.CustomerDaoImpl;
import com.billpay.dao.BusinessDao;
//import com.billpay.dao.CustomerDao;
import com.billpay.dao.TransactionDao;
import com.billpay.dao.TransactionDaoImpl;
import com.billpay.entity.Business;
//import com.billpay.entity.Customer;
import com.billpay.entity.Transaction;
//TODO Change Type to Transaction after current code works
//public class BillpayCallable  implements Callable <Transaction> {



public class BillpayCallableorg  implements Callable <String> {

	
	@Autowired
	private SessionFactory sessionFactory;
	
	TransactionDao transactionDao;
	// store per thread data
	private Business business=null;
	private String name;
	
	public BillpayCallableorg(Business business, String name,	TransactionDao transactionDao)
 {
		this.business = business;
		this.name = name;
		this.transactionDao = transactionDao;
		
	}
	/***
	 /* Pays the bill using input Business object
	 * @return Transaction returns transaction created for the current billpay
	 * 
	 */
	@Override
	public String call() throws Exception {
				payTheBill();
	
		
		return this.name;
	}
	
/*
 *  Uncomment this after current code works fine
	@Override
	public Transaction call() throws Exception {
		payTheBill();
		return createTransaction();
	}
*/
	private  void  payTheBill()  {
		//TODO : Use this.business to implement pay the bill
		System.out.println("this is from call method:"+ name);
	 //business = this.business;
		//synchronized(business){
			
			for(Field field :business.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				String name= field.getName();
				Object value= null;
				try {
					value = field.get(business);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			    System.out.printf("Field name  : %s, value        : %s%n", name, value);

			//}
			
			
		}
		
		Long bsinessId = business.getBusinessId(); 
		String name1=business.getName1();
		BigDecimal checkAmount =  business.getCheckAmount();
		int customerId =  business.getCustomer().getCustomerId();
		
		//Transaction transaction = createTransaction();
			
		Customer customer =new Customer();
		customer.setCustomerId((new Random()).nextInt(70) +1 );
		customer.setAddress("hyd---");
		customer.setName("san---");
		
		 Transaction transaction = new Transaction();
		 transaction.setTransactionId(new Long((new Random()).nextInt(120) +1 ));
		 transaction.setCustomer(customer);
		 transaction.setAmount1(new BigDecimal(7000));
		 transaction.setAfterBalance(new BigDecimal(2000));
		 transaction.setPreviousBalance(new BigDecimal(7000));
		 transaction.setStatus1("y");
		transaction.setTransactionTime(new Date(2015-07-15));
		 transaction.setType0("pay");
		transactionDao.saveTransaction(transaction);

	}
		 
		 //CustomerDao.saveCustomer(customer);
		 
		 //transaction.setType0("pay");
			//transactionDao.saveTransaction(transaction);

		//sessionFactory.getCurrentSession().save(transaction);

		 //System.out.println("For tranID "+ transaction.getTransactionId() + " customer Id is " + business.getCustomer().getCustomerId() +
			//		"amount to be paid is... " +  business.getCheckAmount());
			//transactionDao.saveTransaction(transaction);

	/*	Customer customer =new Customer();
		customer.setCustomerId(3);
		customer.setAddress("hyd---");
		customer.setName("san---");
		
		 Transaction transaction = new Transaction();
		 transaction.setTransactionId(new Long((new Random()).nextInt(120) +1 ));
		 transaction.setCustomer(customer);
		 transaction.setAmount1(new BigDecimal(700));
		 transaction.setAmount1(new BigDecimal(7000));
		 transaction.setAfterBalance(new BigDecimal(2000));
		 transaction.setPreviousBalance(new BigDecimal(3000));
		 transaction.setAfterBalance(new BigDecimal(2000));
		 transaction.setPreviousBalance(new BigDecimal(7000));
		 transaction.setStatus1("y");
		transaction.setTransactionTime(new Date(2015-07-15));
		 transaction.setType0("pay");
		transactionDao.saveTransaction(transaction);

		//transactionDao.saveTransaction(transaction);	
		System.out.println("For businessID of "+ business.getBusinessId() + " business name is " + business.getName1() +
				"amount to be paid is... " +  business.getCheckAmount());
		//synchronized(transactionDao) {
		//transactionDao.saveTransaction(transaction);
		//}
		
		/*Customer customer = new Customer();
		customer.setCustomerId(13);
		
		customer.setName("san13");
		customer.setAddress("hyd");
		//sessionFactory.getCurrentSession().save(customer);
		
		*/
	
	

	private Transaction createTransaction() {
		//TODO : Use DAO to create transaction
		
		
		return null;
	}

}
