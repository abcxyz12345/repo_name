package com.billpay.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.billpay.entity.Business;
import com.billpay.entity.Transaction;

public class BillpayCallable<Transaction> implements Callable {
	String name;
	List<Transaction> al = new ArrayList<Transaction>();

	BillpayCallable(Business business, String name) {
		this.name = name;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Transaction call() throws Exception {
		System.out.println(name);
		Thread.sleep(1000);
		// operations on transactions or calling dao
		return (Transaction) al;

	}

}
