package com.billpay.rest;
import javax.inject.Inject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.dao.BusinessDao;
import com.billpay.entity.Business;

@Component
@Path("/business")
public class BusinessService {
	
	@Autowired
	private BusinessDao businessDao; 
	//@Autowired
	private ModelMapper modelMapper;
	

	@GET @Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	@Transactional
	public Response get(@PathParam("id") int id) {
		
		Business business = businessDao.getBusiness(new Long(id));
		if(business == null) {
			return Response
					.ok("can not find Business with id= "+id)
					.build(); 
		} 
		else {
			return Response
					.ok("business id is     :" + business.getBusinessId() +"   name is     :" + business.getName1()
							+ "business address is:  " +  business.getAddress1())
					.build();
		}
	
		
	}
}
