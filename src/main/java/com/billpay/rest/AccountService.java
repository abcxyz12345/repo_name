package com.billpay.rest;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.dao.AccountDao;
//import com.billpay.entity.Customer;
import com.billpay.entity.Account;


@Component
@Path("/account")

public class AccountService {

	@Autowired
	private AccountDao accountDao; 
	//@Autowired
	private ModelMapper modelMapper;
	

	@GET @Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	@Transactional
	public Response get(@PathParam("id") int id) {
		
		Account account = accountDao.getAccount(new Long(id));
		if(account == null) {
			return Response
					.ok("can not find account with id="+id)
					.build(); 
		} else {
			return Response
					.ok(account.getAccountNumber()+"    :    "+ account.getBalanceAmount())
					.build();
		}
	
		
	}	
	
}
