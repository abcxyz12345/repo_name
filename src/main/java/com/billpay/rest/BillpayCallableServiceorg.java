package com.billpay.rest;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.entity.Business;
import com.billpay.entity.Customer;
import com.billpay.entity.Transaction;
import com.billpay.dao.BusinessDao;
import com.billpay.dao.BusinessDaoImpl;
import com.billpay.dao.CustomerDao;
import com.billpay.dao.PagedDao;
import com.billpay.dao.TransactionDao;

@Path("billpaycallorg")
public class BillpayCallableServiceorg {
	@Autowired
	TransactionDao transactionDao;
	@Autowired
	private BusinessDao businessDao;
	@Autowired
	private CustomerDao customerDao; 
	// @Autowired billpaycallorg/display
	private ModelMapper modelMapper;

	@SuppressWarnings("unused")
	@GET
	@Path("/display")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public Response displayResult() throws InterruptedException,
			ExecutionException {

		return Response.ok(prepareResponse(execute())).build();

	}

	/***
	 * Builds response string from list of results stored in Future<String>
	 * 
	 * @param results
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private String prepareResponse(List<Future<String>> results)
			throws InterruptedException, ExecutionException {
		StringBuilder sb = new StringBuilder();
		for (Future<String> f : results) {
			sb.append("name=").append(f.get()).append("\n");
		}
		return sb.toString();
	}

	/***
	 * create schedular execturor
	 */

	private final ScheduledExecutorService scheduler = Executors
			.newScheduledThreadPool(10);

	/***
	 * Get bill pays from the database and make bill pays in concurrent manner
	 * 
	 * @return List<Future<Transaction>
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@SuppressWarnings("deprecation")
	public List<Future<String>> execute() throws InterruptedException,
			ExecutionException {

		/*
		 * list of billpays to be made - each business object has information
		 * about the bill payment List<Business> businessList = new
		 * ArrayList<Business>(); for (int i = 1; i < 10; i++) {
		 * businessList.add(new Business()); public abstract List<T>
		 * getListOfObjects(Class myclass);dao
		 * 
		 * @Override public List getListOfObjects(Class myclass) { List<T> list
		 * = session.createCriteria(myclass).setResultTransformer(Criteria.
		 * DISTINCT_ROOT_ENTITY).list(); Collections.sort(list); return list;
		 * }impl
		 * 
		 * @SuppressWarnings("unchecked")
		 * 
		 * @Autowired protected SessionFactory sessionFactory;
		 * 
		 * protected Class<T> entity; public List<T> getAll() { List<T> result =
		 * (List<T>) getSessionFactory().getCurrentSession().createQuery("from "
		 * + entity.getName()).list(); return result; }
		 * 
		 * 
		 * 
		 * }
		 */
		
		List<Business> businessList = new ArrayList<Business>();
		businessList = businessDao.getAllEntities();

		for (int i = 0; i < businessList.size(); i++) {
			System.out.println(businessList.get(i));

		}
		

		// storage for Transaction Futures
		// List<Future<Transaction>> futures= new ArrayList
		// <Future<Transaction>> ();
		//List<Future<Transaction>> futuretn= new ArrayList<Future<Transaction>> ();

		
		List<Future<String>> futures = new ArrayList<Future<String>>();

		// submit Callable for each billpay to be made
		for (int i = 0; i < businessList.size(); i++) {

			BillpayCallableorg billpayCallable = new BillpayCallableorg(
					businessList.get(i), "Sanjay-" + i,transactionDao);

			Future<String> future = scheduler.submit(billpayCallable);
			futures.add(future);
		}

		for (Future<String> f : futures) {
			printResult(f);
		}
		
		
		return futures;
	}

	private void printResult(Future<String> f) throws InterruptedException,
			ExecutionException {
		//System.out.println(f.get());

	}
}
