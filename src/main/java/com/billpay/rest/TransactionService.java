package com.billpay.rest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.dao.TransactionDao;
import com.billpay.entity.Transaction;

@Component
@Path("/transaction")

public class TransactionService {
	@Autowired
	private TransactionDao TransactionDao; 
	//@Autowired
	private ModelMapper modelMapper;
	

	@GET @Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	@Transactional
	public Response get(@PathParam("id") int id) {
		
		Transaction transaction = TransactionDao.getTransaction(new Long(id));
		if(transaction == null) {
			return Response
					.ok("can not find Transaction with id="+id)
					.build(); 
		} else {
			return Response
					.ok("Tn id is     :" + transaction.getTransactionId()+"   amount is    :"+transaction.getTransactionId()
							+ "tn     time:" +  transaction.getTransactionTime())
					.build();
		}
	
		
	}
}
