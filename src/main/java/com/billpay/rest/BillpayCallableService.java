/*package com.billpay.rest;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.entity.Transaction;

@Path("billpaycall")
public class BillpayCallableService {

	@Autowired
	SessionFactory sessionFactory;

	private final ScheduledExecutorService scheduler = Executors
			.newScheduledThreadPool(1);

	List<Transaction> al = new ArrayList<Transaction>();

	@SuppressWarnings({ "unused", "rawtypes" })
	@GET
	@Path("/display")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public Response displayResult() throws InterruptedException,
			ExecutionException {
		for (int i = 1; i < 10; i++) {

			Future<Transaction> future = scheduler.submit(new BillpayCallable("babu"	+"  " + i));

			// retrieve value from future object billpaycall/display
			Transaction ret = future.get();

			al.add(ret);
		}

		if (al == null) {
			return Response.ok("can not find ").build();
		} else {
			// http://localhost:8080/billpayph2/webapi/billpaycall/displaytn

			return Response.ok("ok call successfully executed").build();
		}

	}

}
*/