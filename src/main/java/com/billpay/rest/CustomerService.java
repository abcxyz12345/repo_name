package com.billpay.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.dao.CustomerDao;
import com.billpay.entity.Customer;




@Component
@Path("/customer")
public class CustomerService {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CustomerDao customerDao; 
	//@Autowired
	private ModelMapper modelMapper;
	

	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	@Transactional
	public Response get(@PathParam("id") int id) {
		
		Customer customer = new Customer();
		customer.setCustomerId(id);
		customer.setAddress("hyd---");
		customer.setName("san---");
		//Customer customer = customerDao.getCustomer(id);
		
		
		//sessionFactory.getCurrentSession().save(customer);
		customerDao.saveCustomer(customer);

		customer = customerDao.getCustomer(id);
		
		if(customer == null) {
			return Response
					.ok("can not find customer with id="+id)
					.build(); 
		} else {
			return Response
					.ok("nane      :" + customer.getName()+"address    :"+customer.getAddress())
					.build();
		}
	
		
	}
	
}
