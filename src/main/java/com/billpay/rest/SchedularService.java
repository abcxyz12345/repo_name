package com.billpay.rest;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.billpay.entity.Transaction;


@Path("schedulartn")
public class SchedularService {
	
	@Autowired
	SessionFactory sessionFactory;
	
	private final ScheduledExecutorService scheduler =
		     Executors.newScheduledThreadPool(1);
	
	
	@SuppressWarnings("unused")
	@GET
	@Path("/displaytn")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public void displayResult() {

 	   final List<Transaction> al = new ArrayList<Transaction>();
 	   //callable call
		 final Runnable display = new Runnable() {
		       public void run() {
		    	   
		    	   System.out.println("values");
		    	   
		    	   // db read select * from transactions; schedulartn/displaytn
		    	   
		    	   Session session =  sessionFactory.openSession();
		    	   
		    	   al.add((Transaction) session.createSQLQuery("SELECT * " + "from transaction"));
		    	   
		    	   
		    	   }
		         };
		         
		      
		         
		  //what cmd(runnable display) should be executed with first delay,next delays and unit of time       
		         
		  final ScheduledFuture<?> displayHandle = scheduler.scheduleAtFixedRate(display, 3, 17, SECONDS);
		  
		  // up to what time it should be displayed in this case 15 min..ie run() life ..if we don't 
		  //use the following code thread will run indefinitely.
		  
		 /* scheduler.schedule(new Runnable() {
		       public void run() { displayHandle.cancel(true); }
		     }, 15 * 60, SECONDS);*/
		  
		  /*
		  if(al == null) {
				return Response
						.ok("can not find records")
						.build(); 
			} else {
				//http://localhost:8080/billpayph2/webapi/schedulartn/displaytn
				
				return Response.ok("ok").build();
			}*/
		
		  
}
}