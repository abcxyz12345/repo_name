
http://localhost:8080/billpayph2/webapi/billpaycall/display
ok call successfully executed


everyday read business table at 12.00am
select * from business where (startdate is < currentdate and dayofmonth < currentdate);






    for each businessId 
    		check transaction status in transaction table
    				if status is 's' leave that businessId
    				else pay the bill and write the transaction entry and update account table;
    end   
	
    Billpaycallableorg.java
    display Object values

for  business object
getting field names and field values:
for(Field field :business.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				String name= field.getName();
				Object value= null;
				try {
					value = field.get(business);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// If field.getName();
//returns Business type then business.getBusinessId(); print that else next printf statement
			    System.out.printf("Field name  : %s, value        : %s%n", name, value);

			}
//topics to be discussed			
1)line no71:		value = field.get(business);//for businessId it is returning memory address? // If field.getName();
//returns Business type then business.getBusinessId(); print that else next printf statement
2) transactionDao interface ---why notImpl here where is the Object? autowired :diff bwn sessionFactory and transactionDao?
3) I tried using synchronized keyword but still giving errors;


call()
submit()
callable

http://localhost:8080/j-s-h-final/webapi/customer/123
Can not find customer with id 123


Context.xml 
<Resource name="jdbc/billpay" auth="Container" type="javax.sql.DataSource"
               maxActive="50" maxIdle="30" maxWait="10000"
               username="root" password="12345" 
               driverClassName="com.mysql.jdbc.Driver"
               url="jdbc:mysql://localhost:3306/billpay"/>


applicationContext.xml

<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.2.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.0.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.0.xsd">

    <context:component-scan base-package="com.billpay.*" />
    <context:component-scan base-package="com.avenuecode.jchallenge.*" />
    <tx:annotation-driven/>
    
    <bean id="transactionManager" class="org.springframework.orm.hibernate4.HibernateTransactionManager">
        <property name="sessionFactory" ref="sessionFactory"/>
    </bean>
    <bean id="sessionFactory" class="org.springframework.orm.hibernate4.LocalSessionFactoryBean">
       
        <property name="annotatedClasses">
         <list>
            <value>com.billpay.entity.Customer</value>
         </list>
        </property>
         <property name="hibernateProperties">
            <props>
                <prop key="hibernate.hbm2ddl.auto">create-drop</prop>
                <prop key="hibernate.transaction.factory_class">org.hibernate.transaction.JDBCTransactionFactory</prop>
                <prop key="hibernate.dialect">org.hibernate.dialect.MySQL5Dialect</prop>
                <prop key="hibernate.connection.isolation">2</prop>
                <prop key="hibernate.connection.autocommit">false</prop>
                <prop key="hibernate.generate_statistics">true</prop>
                <prop key="hibernate.show_sql">true</prop>
                <prop key="hibernate.format_sql">true</prop>
                <prop key="hibernate.use_sql_comments">true</prop>
                <prop key="hibernate.jdbc.fetch_size">100</prop>
                <prop key="hibernate.jdbc.batch_size">30</prop>
                <prop key="hibernate.order_inserts">true</prop>
                <prop key="hibernate.order_updates">true</prop>
                <prop key="hibernate.max_fetch_depth">3</prop>
            </props>
        </property>
         <property name="dataSource" ref="dataSource"/>
       
    </bean>
 <bean id="dataSource" class="org.springframework.jndi.JndiObjectFactoryBean">
        <property name="jndiName" value="java:comp/env/jdbc/billpay" />
        <property name="resourceRef" value="true"/>
        <property name="lookupOnStartup" value="false"/>
        <property name="proxyInterface" value="javax.sql.DataSource"/>
        <property name="cache" value="false"/>
    </bean>
  
</beans>
"Phase two - finish query services, POC for scheduler
 write all query services
 (use insert SQLs to insert/update 
  run basic (POC) scheduler"				
Tasks	Status	Estimated finish date	Actual Finish date	Issues/ Problems
	"Phase one: POC for services
  (create customer query service with hibernate/spring/jax-rs,  querying database)"				
Tasks	Status	Estimated finish date	Actual Finish date	Issues/ Problems



Project Phases:

				
1       Create Database				
1.1      prepare schema, SQLs	done	28/Jul	28/Jul	
1.2      create database	done	28/Jul	28/Jul	
				
2       Write Services (customer)				
2.1      hibernate setup				
2.2      setup Spring, jax-rs				
2.3.1.1    customer - write entity				
2.3.3.1     customer - write jax-rc service				
				
				
Phase one Deliverables				
project billpay-phaseone				
SQLs if any				
				
2.4.1.  write insert SQLs		29/Jul		
2.3      write query services		29/Jul		
2.3.1      write entity objects		29/Jul		
2.3.1.1     customer		29/Jul		
2.3.1.2     business		29/Jul		
2.3.1.3     billpay		29/Jul		
2.3.2      Write json for		29/Jul		
2.3.2.1     customer		29/Jul		
2.3.2.2     business		29/Jul		
2.3.2.3     billpay		29/Jul		
2.3.3      write jax-rs query services		29/Jul		
2.3.3.1     customer		29/Jul		
2.3.3.2     business		29/Jul		
2.3.3.3     billpay		29/Jul		
2.3.3.4     transaction		29/Jul		
4       Scheduler				
4.1      Thread pool for scheduler		29/Jul		
4.2      identify (query) billpays to be sent		29/Jul		
4.3      Send billpays  ( log transaction)		29/Jul		
				
Phase two Deliverables				
project billpay-phasetwo				
insert SQLs				


Phase three  - add UI, write insert/query services				
Tasks	Status	Estimated finish date	Actual Finish date	Issues/ Problems
				
3       Develop UI for 				
3.1      add/modify customers		30/Jul		
3.2      add/modify business		30/Jul		
3.3      add/modify billet		30/Jul		
3.4      show scheduled bill pays sorted by future send date		30/Jul		
2.4      write insert/ipdate services		30/Jul		
2.4.1.  write insert SQLs		30/Jul		
2.4.2      customer		30/Jul		
2.4.3      business		30/Jul		
2.4.4      billpay		30/Jul		
2.4.5      scheduler service		30/Jul		
				
Phase two Deliverables				
project billpay-phase-three				
insert SQLs				




